export type  TrackRecord = {
  id: string;
  startedAt: Date;
  finishedAt?: Date;
  customer: string;
  project: string;
  ticket?: string;
  workNotes?: string;
}

export interface Customer {
  id: string,
  name: string,
}

export interface Project {
  id: string,
  name: string,
}

export interface User {
  userId: string,
  username: string,
}

export interface Task {
  id: string,
  title: string
}
