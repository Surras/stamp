FROM node:14.19.1-alpine

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install --production

COPY ./dist/apps/ ./
CMD node api/main.js
