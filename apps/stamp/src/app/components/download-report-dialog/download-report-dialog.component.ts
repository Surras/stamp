import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { DateTime } from 'luxon';

@Component({
  selector: 'stamp-download-report-dialog',
  templateUrl: './download-report-dialog.component.html',
  styleUrls: ['./download-report-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownloadReportDialogComponent implements OnInit {
  months = [
    'january',
    'february',
    'march',
    'april',
    'may',
    'june',
    'july',
    'august',
    'september',
    'october',
    'november',
    'december',
  ];

  selectedMonth = 0;

  constructor(
    protected dialogRef: NbDialogRef<{ month: string } | undefined>
  ) {}

  ngOnInit(): void {
    this.selectedMonth = DateTime.now().month - 1;
  }

  close(input: 'download' | undefined) {
    this.dialogRef.close(input && { month: this.selectedMonth + 1 });
  }

  stepMonth(direction: 'back' | 'forward') {
    if (direction === 'forward') {
      this.selectedMonth =
        this.selectedMonth + 1 < this.months.length
          ? this.selectedMonth + 1
          : 0;
    } else {
      this.selectedMonth =
        this.selectedMonth === 0
          ? this.months.length - 1
          : this.selectedMonth - 1;
    }

    console.log('monthIdx', this.selectedMonth);
  }
}
