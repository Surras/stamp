import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Customer, Project, TrackRecord } from '@stamp/api-interfaces';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DateTime } from 'luxon';

@Component({
  selector: 'stamp-record-form-dialog',
  templateUrl: './record-form-dialog.component.html',
  styleUrls: ['./record-form-dialog.component.scss'],
})
export class RecordFormDialogComponent {
  @Input()
  mode?: 'extended' | 'simple';

  @Input()
  title = 'Track Record';

  @Input()
  set record(value: TrackRecord | undefined) {
    if (value) {
      console.log(value);
      this.form.setValue({
        startDate: value.startedAt,
        startTime: value.startedAt,
        finishDate: value.finishedAt,
        finishTime: value.finishedAt,
        customer: value.customer || '',
        project: value.project || '',
        workNotes: value.workNotes || '',
        ticket: value.ticket || '',
      });
    }
  }

  @Input()
  customers?: ReadonlyArray<Customer>;

  @Input()
  projects?: ReadonlyArray<Project>;

  form: FormGroup;

  constructor(
    protected nbDialogRef: NbDialogRef<TrackRecord | undefined>,
    formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group({
      startDate: new FormControl(),
      startTime: new FormControl(),
      finishDate: new FormControl(),
      finishTime: new FormControl(),
      customer: new FormControl(),
      project: new FormControl(),
      ticket: new FormControl(),
      workNotes: new FormControl(),
    });
  }

  close() {
    this.nbDialogRef.close();
  }

  discard() {
    this.nbDialogRef.close(undefined);
  }

  save() {
    if (this.mode === 'extended') {
      const startTime = DateTime.fromJSDate(this.form.value.startTime);
      const startedAt = DateTime.fromJSDate(this.form.value.startDate).set({
        hour: startTime.hour,
        minute: startTime.minute,
        second: startTime.second,
      });

      const finishTime = DateTime.fromJSDate(this.form.value.finishTime);
      const finishedAt = DateTime.fromJSDate(this.form.value.finishDate).set({
        hour: finishTime.hour,
        minute: finishTime.minute,
        second: finishTime.second,
      });
      this.nbDialogRef.close({
        startedAt: startedAt.toJSDate(),
        finishedAt: finishedAt.toJSDate(),
        customer: this.form.value.customer?.length
          ? this.form.value.customer
          : undefined,
        project: this.form.value.project?.length
          ? this.form.value.project
          : undefined,
        workNotes: this.form.value.workNotes,
        ticket: this.form.value.ticket,
      });
    } else if (this.mode === 'simple') {
      this.nbDialogRef.close({
        customer: this.form.value.customer?.length
          ? this.form.value.customer
          : undefined,
        project: this.form.value.project?.length
          ? this.form.value.project
          : undefined,
        workNotes: this.form.value.workNotes,
        ticket: this.form.value.ticket,
      });
    }
  }
}
