import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordFormDialogComponent } from './record-form-dialog.component';

describe('RecordFormDialogComponent', () => {
  let component: RecordFormDialogComponent;
  let fixture: ComponentFixture<RecordFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecordFormDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
