import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDiscardDialogComponent } from './confirm-discard-dialog.component';

describe('ConfirmDiscardDialogComponent', () => {
  let component: ConfirmDiscardDialogComponent;
  let fixture: ComponentFixture<ConfirmDiscardDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmDiscardDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDiscardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
