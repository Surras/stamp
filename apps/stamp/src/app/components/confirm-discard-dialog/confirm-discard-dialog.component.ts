import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'stamp-confirm-discard-dialog',
  templateUrl: './confirm-discard-dialog.component.html',
  styleUrls: ['./confirm-discard-dialog.component.scss'],
})
export class ConfirmDiscardDialogComponent {
  @Input()
  title = '';

  @Input()
  text?: string;

  @Input()
  confirmButtonText = 'confirm';

  @Input()
  discardButtonText = 'abort';

  constructor(protected dialogRef: NbDialogRef<'confirm' | 'discard'>) {}

  close(input: 'confirm' | 'discard') {
    this.dialogRef.close(input);
  }
}
