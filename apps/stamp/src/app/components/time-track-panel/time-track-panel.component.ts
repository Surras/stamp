import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import {
  interval,
  map,
  Observable,
  of,
  shareReplay,
  withLatestFrom,
} from 'rxjs';
import { DateTime, Duration } from 'luxon';
import { RecordService } from '../../services/record.service';
import { NbDialogService, NbPopoverDirective } from '@nebular/theme';
import { Customer, Project } from '@stamp/api-interfaces';
import { TrackRecordInfo } from '../../types';
import { RecordFormDialogComponent } from '../record-form-dialog/record-form-dialog.component';
import { ConfirmDiscardDialogComponent } from '../confirm-discard-dialog/confirm-discard-dialog.component';

@Component({
  selector: 'stamp-time-track-panel',
  templateUrl: './time-track-panel.component.html',
  styleUrls: ['./time-track-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeTrackPanelComponent {
  @ViewChild(NbPopoverDirective) popover?: NbPopoverDirective;

  duration$: Observable<string>;
  running$: Observable<boolean>;

  recordFormOpen = false;

  customers$: Observable<ReadonlyArray<Customer>>;
  projects$: Observable<ReadonlyArray<Project>>;
  currentRecordInfo$: Observable<TrackRecordInfo | undefined>;

  constructor(
    private recordService: RecordService,
    private dialogService: NbDialogService
  ) {
    this.running$ = this.recordService.runningStatus$();
    this.customers$ = of([]);
    this.projects$ = of([]);
    this.currentRecordInfo$ = this.recordService.currentRecordInfo$().pipe(
      shareReplay({
        bufferSize: 1,
        refCount: false,
      })
    );

    // TODO: Interval muss angehalten werden wenn nicht benötigt
    this.duration$ = interval(1000)
      .pipe(withLatestFrom(this.recordService.currentRecord$))
      .pipe(map(([_, record]) => record))
      .pipe(
        map((record) =>
          record
            ? Math.round(
                DateTime.now().diff(DateTime.fromJSDate(record.startedAt), [
                  'seconds',
                ]).seconds
              )
            : 0
        )
      )
      .pipe(
        map((seconds) => {
          const dur = Duration.fromObject({ seconds });
          return `${dur.toFormat('hh:mm:ss')} h`;
        })
      );
  }

  start() {
    this.dialogService
      .open(RecordFormDialogComponent, {
        context: { mode: 'simple', title: 'Start Stopwatch' },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.recordService.updateRecordInfo(result);
          this.recordService.startRecord();
        }
      });
  }

  stop() {
    this.recordService.stopRecord();
  }

  switch() {
    this.dialogService
      .open(RecordFormDialogComponent, {
        context: { mode: 'simple', title: 'Switch Record' },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.recordService.stopRecord();
          this.recordService.updateRecordInfo(result);
          this.recordService.startRecord();
        }
      });
  }

  discardRequested() {
    this.dialogService
      .open(ConfirmDiscardDialogComponent, {
        context: {
          title: 'Really discard current running record?',
          confirmButtonText: 'yes, discard',
        },
      })
      .onClose.subscribe(() => {
        this.recordService.discard();
      });
  }
}
