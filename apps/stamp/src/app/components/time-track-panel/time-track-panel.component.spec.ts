import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeTrackPanelComponent } from './time-track-panel.component';

describe('TimeTrackPanelComponent', () => {
  let component: TimeTrackPanelComponent;
  let fixture: ComponentFixture<TimeTrackPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeTrackPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeTrackPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
