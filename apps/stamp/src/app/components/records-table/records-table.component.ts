import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { BehaviorSubject, filter, map, Subscription } from 'rxjs';
import { RecordService } from '../../services/record.service';
import { TrackRecord } from '@stamp/api-interfaces';
import { NbDialogService, NbMenuService } from '@nebular/theme';
import { ConfirmDiscardDialogComponent } from '../confirm-discard-dialog/confirm-discard-dialog.component';
import { DateTime } from 'luxon';
import { RecordFormDialogComponent } from '../record-form-dialog/record-form-dialog.component';

@Component({
  selector: 'stamp-records-table',
  templateUrl: './records-table.component.html',
  styleUrls: ['./records-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordsTableComponent implements OnInit, OnDestroy {
  @Input()
  set records(value: Array<TrackRecord>) {
    const grouped = value.reduce((prev, curr) => {
      const timestamp = DateTime.fromJSDate(curr.startedAt).toLocaleString({
        ...DateTime.DATE_SHORT,
        weekday: 'long',
      });

      let group = prev.find((g) => g.title === timestamp);
      if (!group) {
        group = { title: timestamp, entries: [] };
        prev.push(group);
      }

      group.entries.push(curr);
      return prev;
    }, [] as Array<{ title: string; entries: Array<TrackRecord> }>);

    this.groupedRecordsSubject.next(grouped);
  }

  @Output()
  refreshRequested = new EventEmitter();

  recordMenuOptions = [{ title: 'Edit' }, { title: 'Delete' }];

  #subscriptions = new Subscription();

  groupedRecordsSubject = new BehaviorSubject<
    Array<{ title: string; entries: Array<TrackRecord> }>
  >([]);

  constructor(
    private recordService: RecordService,
    private nbMenuService: NbMenuService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit(): void {
    this.#subscriptions.add(
      this.nbMenuService
        .onItemClick()
        .pipe(
          filter(({ tag }) => tag.startsWith('record_')),
          map(({ tag, item }) => ({
            tag: tag.slice(tag.indexOf('_') + 1),
            option: item.title,
          }))
        )
        .subscribe((v) => {
          const record = this.groupedRecordsSubject.value
            .reduce(
              (prev, curr) => prev.concat(curr.entries),
              [] as Array<TrackRecord>
            )
            .find((rec) => rec.id === v.tag);
          if (record) {
            switch (v.option) {
              case 'Delete':
                this.dialogService
                  .open(ConfirmDiscardDialogComponent, {
                    context: {
                      title: 'Really delete Record?',
                      confirmButtonText: 'yes, delete',
                      text: formatRecordToString(record),
                    },
                  })
                  .onClose.subscribe((result) => {
                    if (result === 'confirm') {
                      this.recordService.remove(v.tag);
                    }
                  });
                break;
              case 'Edit':
                this.dialogService
                  .open(RecordFormDialogComponent, {
                    context: {
                      record: record,
                      mode: 'extended',
                      title: 'Edit Record',
                    },
                  })
                  .onClose.subscribe((result) => {
                    this.recordService.updateRecord(v.tag, result);
                  });
                break;
              default:
                break;
            }
          } else {
            // TODO: show error notification
            console.error('no record found with id ', v.tag);
          }
        })
    );
  }

  ngOnDestroy(): void {
    this.#subscriptions.unsubscribe();
  }
}

function formatRecordToString(record: TrackRecord) {
  const lines: Array<string> = [];
  lines.push(
    `Start: ${DateTime.fromJSDate(record.startedAt).toFormat(
      'dd.MM.yyyy hh:mm'
    )} Uhr`
  );
  lines.push(
    `Finished: ${
      record.finishedAt
        ? DateTime.fromJSDate(record.finishedAt).toFormat('dd.MM.yyyy hh:mm') +
          ' Uhr'
        : 'not finished yet'
    }`
  );
  lines.push(`Ticket: ${record.ticket}`);
  lines.push(`Notes: ${record.workNotes}`);

  return lines.join('\n');
}
