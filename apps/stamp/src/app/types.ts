import {TrackRecord} from '@stamp/api-interfaces';

export interface User {
  userId: string;
  username: string;
  accessToken: string;
}

export type TrackRecordInfo = Pick<TrackRecord, 'customer' | 'project' | 'workNotes' | 'ticket'>;
