import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TimeTrackPanelComponent } from './components/time-track-panel/time-track-panel.component';
import {
  NbAccordionModule,
  NbButtonGroupModule,
  NbButtonModule,
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbSelectModule,
  NbThemeModule,
  NbTimepickerModule,
  NbToastrModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { RouterModule, Routes } from '@angular/router';
import { RecordsTableComponent } from './components/records-table/records-table.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalcDurationPipe } from './utils/calc-duration.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginView } from './views/login/login.view.';
import { AuthGuard } from './guards/auth.guard';
import { RecordView } from './views/record/record.view';
import { JwtInterceptor } from './core-utils/jwt-interceptor';
import { ErrorInterceptor } from './core-utils/error-interceptor';
import { ConfirmDiscardDialogComponent } from './components/confirm-discard-dialog/confirm-discard-dialog.component';
import { RecordFormDialogComponent } from './components/record-form-dialog/record-form-dialog.component';
import { API_URL_TOKEN } from './injection-tokens';
import { environment } from '../environments/environment';
import { ProfileView } from './views/profile/profile.view';
import { DownloadReportDialogComponent } from './components/download-report-dialog/download-report-dialog.component';

const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: RecordView },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileView },
  { path: 'login', component: LoginView },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [
    AppComponent,
    TimeTrackPanelComponent,
    RecordsTableComponent,
    CalcDurationPipe,
    LoginView,
    RecordView,
    ConfirmDiscardDialogComponent,
    RecordFormDialogComponent,
    ProfileView,
    DownloadReportDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, { useHash: false }),
    NbLayoutModule,
    NbThemeModule.forRoot(),
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbListModule,
    NbCardModule,
    NbContextMenuModule,
    NbMenuModule.forRoot(),
    NbAccordionModule,
    NbPopoverModule,
    ReactiveFormsModule,
    NbTooltipModule,
    NbFormFieldModule,
    NbUserModule,
    NbDialogModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbTimepickerModule.forRoot(),
    NbButtonGroupModule,
    NbToastrModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: API_URL_TOKEN,
      useValue: environment.production
        ? 'https://stamp.ironic-dev.de/api'
        : 'http://localhost:3333/api',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
