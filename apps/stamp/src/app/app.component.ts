import { Component, OnInit } from '@angular/core';
import { filter, map, Observable } from 'rxjs';
import { AuthService } from './services/auth.service';
import { NbMenuService } from '@nebular/theme';
import { Router } from '@angular/router';
import { User } from './types';

@Component({
  selector: 'stamp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  currentUser$: Observable<User | undefined>;

  USER_MENU_TAG = 'USER_MENU_TAG';
  menuItems = [{ title: 'Home' }, { title: 'Profile' }, { title: 'Logout' }];

  constructor(
    private authService: AuthService,
    private nbMenuService: NbMenuService,
    private router: Router
  ) {
    this.currentUser$ = authService.currentUserSubject;
  }

  ngOnInit(): void {
    this.nbMenuService
      .onItemClick()
      .pipe(filter(({ tag }) => tag === this.USER_MENU_TAG))
      .pipe(map(({ item: { title } }) => title))
      .subscribe((val) => {
        switch (val) {
          case 'Home':
            void this.router.navigate(['/']);
            break;
          case 'Profile':
            void this.router.navigate(['/profile']);
            break;
          case 'Logout':
            this.authService.logout();
            void this.router.navigate(['/login']);
            break;
        }
      });
  }
}
