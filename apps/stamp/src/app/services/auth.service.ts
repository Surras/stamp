import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map } from 'rxjs';
import { User } from '../types';
import { API_URL_TOKEN } from '../injection-tokens';

@Injectable({ providedIn: 'root' })
export class AuthService {
  currentUserSubject = new BehaviorSubject<User | undefined>(undefined);

  constructor(
    private http: HttpClient,
    @Inject(API_URL_TOKEN) private apiUrl: string
  ) {
    const accessToken = localStorage.getItem('currentUser');
    if (accessToken) {
      const token = decodeToken(accessToken);
      this.currentUserSubject.next(token);
    }
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(`${this.apiUrl}/auth/login`, { username, password })
      .pipe(
        map((data) => {
          if (data && data.access_token) {
            const token = decodeToken(data.access_token);
            localStorage.setItem('currentUser', token.accessToken);
            this.currentUserSubject.next(token);
          }
          return data;
        })
      );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(undefined);
  }
}

function decodeToken(token: string): { username: string, userId: string, accessToken: string } {
  const decodedToken = JSON.parse(atob(token.split('.')[1]));
  return {
    userId: decodedToken.sub,
    username: decodedToken.username,
    accessToken: token
  };
}
