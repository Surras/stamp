import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, Subject, switchMap } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { TrackRecord } from '@stamp/api-interfaces';
import { TrackRecordInfo } from '../types';
import { API_URL_TOKEN } from '../injection-tokens';

@Injectable({
  providedIn: 'root',
})
export class RecordService {
  records$: Observable<Array<TrackRecord>>;
  currentRecord$ = new BehaviorSubject<TrackRecord | undefined>(undefined);
  private currentRecordInfo = new BehaviorSubject<TrackRecordInfo | undefined>(
    undefined
  );
  private runningSubject = new BehaviorSubject(false);
  private refreshSubject = new Subject<void>();

  constructor(
    private http: HttpClient,
    @Inject(API_URL_TOKEN) private apiUrl: string
  ) {
    this.records$ = this.refreshSubject.pipe(
      switchMap(() => this.fetchRecords())
    );
  }

  private fetchRecords(): Observable<Array<TrackRecord>> {
    return this.http.get<Array<TrackRecord>>(`${this.apiUrl}/records`).pipe(
      map((data) => {
        if (Array.isArray(data)) {
          return mapResponseArray(data);
        } else {
          return [];
        }
      })
    );
  }

  refresh() {
    this.refreshSubject.next();
  }

  runningStatus$() {
    return this.runningSubject;
  }

  currentRecordInfo$() {
    return this.currentRecordInfo;
  }

  updateRecordInfo(input: TrackRecordInfo) {
    this.currentRecordInfo.next(input);
  }

  startRecord() {
    if (this.currentRecordInfo.value) {
      const currentVal = {
        ...this.currentRecordInfo.value,
        id: undefined,
        startedAt: undefined,
        finishedAt: undefined,
      };

      this.http
        .post<TrackRecord>(`${this.apiUrl}/records`, currentVal)
        .subscribe({
          next: (val) => {
            this.runningSubject.next(true);
            this.currentRecord$.next(mapResponseTypes(val));
            this.refresh();
          },
          error: (err) => {
            alert(`oh no, error: ${err}`);
          },
        });
    } else {
      // TODO: Error Handling
      console.error('record info not given. Please fill out form');
    }
  }

  stopRecord() {
    this.runningSubject.next(false);
    if (this.currentRecord$.value) {
      const currentRecId = this.currentRecord$.value?.id;
      this.http
        .put<TrackRecord>(`${this.apiUrl}/records/${currentRecId}`, {
          finishedAt: new Date(),
        })
        .subscribe({
          next: () => {
            this.currentRecord$.next(undefined);
            this.refresh();
          },
        });
    }
  }

  discard() {
    const currentId = this.currentRecord$.value?.id;
    if (currentId) {
      this.http.delete(`${this.apiUrl}/records/${currentId}`).subscribe(() => {
        this.runningSubject.next(false);
        this.currentRecord$.next(undefined);
        this.refresh();
      });
    }
  }

  remove(id: string) {
    this.http
      .delete(`${this.apiUrl}/records/${id}`)
      .subscribe(() => this.refresh());
  }

  getLatest() {
    this.http.get<TrackRecord>(`${this.apiUrl}/records/latest`).subscribe(
      (v) => {
        if (v) {
          if (!v.finishedAt) {
            this.currentRecord$.next(mapResponseTypes(v));
          }
          this.currentRecordInfo.next(v);
          this.runningSubject.next(v && v.startedAt && !v.finishedAt);
        }
      },
      (error) => console.log('error while fetching latest', error)
    );
  }

  getRecord(id: string): Observable<TrackRecord | undefined> {
    return this.http.get<TrackRecord | undefined>(
      `${this.apiUrl}/records/${id}`
    );
  }

  updateRecord(id: string, recordInfo: TrackRecord) {
    this.http
      .put<TrackRecord>(`${this.apiUrl}/records/${id}`, recordInfo)
      .subscribe(() => this.refresh());
  }

  createNewRecord(input: Omit<TrackRecord, 'id'>) {
    this.http
      .post<TrackRecord>(`${this.apiUrl}/records`, input)
      .subscribe(() => this.refresh());
  }
}

function mapResponseTypes(input: TrackRecord) {
  return {
    ...input,
    startedAt: new Date(input.startedAt),
    finishedAt: input.finishedAt && new Date(input.finishedAt),
  };
}

function mapResponseArray(input: Array<TrackRecord>) {
  return input.map((i) => mapResponseTypes(i));
}
