import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL_TOKEN } from '../injection-tokens';
import * as FileSaver from 'file-saver';

@Injectable({ providedIn: 'root' })
export class ReportService {
  #hostUrl = '';

  constructor(
    private http: HttpClient,
    @Inject(API_URL_TOKEN) private apiUrl: string
  ) {
    this.#hostUrl = `${this.apiUrl}/report`;
  }

  downloadReport(month: number) {
    this.http
      .get(`${this.#hostUrl}/download?month=${month}`, { responseType: 'blob' })
      .subscribe({
        next: (res) => {
          const blob = new Blob([res], { type: 'text/json; charset=utf-8' });
          FileSaver.saveAs(blob, 'report.json');
        },
        error: (err) => console.log('error while downloading report', err),
      });
  }
}
