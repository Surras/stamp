import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL_TOKEN } from '../injection-tokens';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Injectable({ providedIn: 'root' })
export class UsersService {
  #hostUrl = '';

  constructor(
    private http: HttpClient,
    @Inject(API_URL_TOKEN) private apiUrl: string,
    private toastService: NbToastrService
  ) {
    this.#hostUrl = `${this.apiUrl}/users`;
  }

  async changePassword(input: { oldPassword: string; newPassword: string }) {
    this.http.post(`${this.#hostUrl}/password`, input).subscribe({
      next: () => {
        this.toastService.success(
          'password changed successfully',
          'Password Change',
          { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT }
        );
      },
      error: (err) => {
        this.toastService.danger(
          `Error while changing Password: ${err}`,
          'Password Change',
          { position: NbGlobalPhysicalPosition.BOTTOM_RIGHT }
        );
      },
    });
  }
}
