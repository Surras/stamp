import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';

@Component({
  selector: 'stamp-login',
  templateUrl: './login.view.html',
  styleUrls: ['./login.view.scss'],
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class LoginView {
  form: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group({
      username: new FormControl(),
      password: new FormControl(),
    });
  }

  login() {
    this.authService
      .login(this.form.value.username, this.form.value.password)
      .pipe(first())
      .subscribe({
        next: () => {
          void this.router.navigate(['/']);
        },
        error: (err) => {
          console.error('error while logging in', err);
        },
      });
  }
}
