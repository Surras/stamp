import { Component } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'stamp-profile-view',
  templateUrl: 'profile.view.html',
  styleUrls: ['profile.view.scss'],
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class ProfileView {
  formGroup: FormGroup;

  constructor(
    private usersService: UsersService,
    formBuilder: FormBuilder,
    private toastService: NbToastrService
  ) {
    this.formGroup = formBuilder.group({
      old: new FormControl(),
      newPwd: new FormControl(),
      repeat: new FormControl(),
    });
  }

  async changePasswordRequested() {
    const oldPassword = this.formGroup.value.old;
    const newPassword = this.formGroup.value.newPwd;

    await this.usersService.changePassword({ oldPassword, newPassword });
  }
}
