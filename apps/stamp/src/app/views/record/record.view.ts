import { AfterViewInit, Component } from '@angular/core';
import { catchError, Observable, of } from 'rxjs';
import { RecordService } from '../../services/record.service';
import { TrackRecord } from '@stamp/api-interfaces';
import { DownloadReportDialogComponent } from '../../components/download-report-dialog/download-report-dialog.component';
import { RecordFormDialogComponent } from '../../components/record-form-dialog/record-form-dialog.component';
import { NbDialogService } from '@nebular/theme';
import { ReportService } from '../../services/report.service';

@Component({
  selector: 'stamp-record',
  templateUrl: './record.view.html',
  styleUrls: ['./record.view.scss'],
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class RecordView implements AfterViewInit {
  records$: Observable<Array<TrackRecord>>;

  constructor(
    private recordService: RecordService,
    private reportService: ReportService,
    private dialogService: NbDialogService
  ) {
    this.records$ = this.recordService.records$.pipe(
      catchError((err) => {
        console.log('error while fetching records', err);
        return of([]);
      })
    );
  }

  ngAfterViewInit() {
    this.recordService.getLatest();
    this.recordService.refresh();
  }

  downloadReportRequested() {
    this.dialogService
      .open(DownloadReportDialogComponent)
      .onClose.subscribe((result) => {
        if (result) {
          this.reportService.downloadReport(result.month);
        }
      });
  }

  createNewEntry() {
    this.dialogService
      .open(RecordFormDialogComponent, {
        context: { mode: 'extended', title: 'Create new Record' },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.recordService.createNewRecord(result);
        }
      });
  }

  tickFunc() {
    console.log('tick');
  }

  changeTableView($event: any) {}

  stepRange(direction: 'forward' | 'backward') {}
}
