import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, Observable, throwError} from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(((err: HttpErrorResponse) => {
      if (err.status === 401 && !window.location.href.includes('/login')) {
        this.authService.logout();
        location.reload();
      }

      const error = err.error.error || err.error.message || err.statusText;
      // TODO: notification error
      return throwError(error);
    })));
  }
}
