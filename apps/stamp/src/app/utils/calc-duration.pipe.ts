import {Pipe, PipeTransform} from '@angular/core';
import {DateTime} from 'luxon';

@Pipe({
  name: 'calcDuration'
})
export class CalcDurationPipe implements PipeTransform {

  transform(startedAt: Date, finishedAt: Date | undefined, withSeconds: boolean = false): string {
    return DateTime.fromJSDate(finishedAt || new Date()).diff(DateTime.fromJSDate(startedAt))
      .toFormat(withSeconds ? 'hh:mm:ss' : 'hh:mm');
  }
}
