import {map, MonoTypeOperatorFunction, OperatorFunction} from 'rxjs';

export function isDefined(): MonoTypeOperatorFunction<boolean> {
  return (o) => o.pipe(map(value => value !== undefined));
}
