import {Task} from '@stamp/api-interfaces';

export class TaskDto implements Task {
  id: string;
  title: string;
}
