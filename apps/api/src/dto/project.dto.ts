import {Project} from '@stamp/api-interfaces';

export class ProjectDto implements Project {
  id: string;
  name: string;
}
