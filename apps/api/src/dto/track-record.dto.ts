import {TrackRecord} from '@stamp/api-interfaces';
import {Type} from 'class-transformer';

export class TrackRecordDto implements TrackRecord {
  id: string;

  @Type(() => Date)
  startedAt: Date;

  @Type(() => Date)
  finishedAt?: Date;
  customer: string;
  project: string;
  ticket?: string;
  workNotes?: string;
}
