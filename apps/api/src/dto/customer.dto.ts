import {Customer} from '@stamp/api-interfaces';

export class CustomerDto implements Customer {
  id: string;
  name: string;
}
