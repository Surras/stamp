import {User} from '@stamp/api-interfaces';

export class UserDto implements User {
  userId: string;
  username: string;
}
