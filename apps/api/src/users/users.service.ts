import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>
  ) {}

  async findByUsername(username: string): Promise<UserEntity> {
    return this.userRepository.findOne({ where: { username } });
  }

  async findOne(userId: string): Promise<UserEntity> {
    return this.userRepository.findOne(userId);
  }

  async remove(id: string): Promise<void> {
    await this.userRepository.delete(id);
  }

  async changePassword(
    userId: string,
    input: { oldPassword: string; newPassword: string }
  ) {
    const user = await this.userRepository.findOne(userId);
    if (user.password === input.oldPassword) {
      user.password = input.newPassword;
      await this.userRepository.save(user);
    } else {
      throw new Error('invalid old password');
    }
  }
}
