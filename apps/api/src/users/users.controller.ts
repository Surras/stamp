import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UsersService } from './users.service';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post('/password')
  async downloadReport(
    @Request() req,
    @Body() input: { oldPassword: string; newPassword: string }
  ) {
    const userId = req.user.userId;
    await this.usersService.changePassword(userId, input);
  }
}
