import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { RecordController } from './controller/record/record.controller';
import { RecordService } from './controller/record/record.service';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import { UsersModule } from '../users/users.module';
import { TrackRecordEntity } from '../entities/track-record.entity';
import { CustomerEntity } from '../entities/customer.entity';
import { ProjectEntity } from '../entities/project.entity';
import { TaskEntity } from '../entities/task.entity';
import { CustomerController } from './controller/customer/customer.controller';
import { CustomerService } from './controller/customer/customer.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ReportController } from './controller/report/report.controller';
import { ReportService } from './controller/report/report.service';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'stamp'),
    }),
    AuthModule,
    UsersModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        const entities = [
          UserEntity,
          TrackRecordEntity,
          CustomerEntity,
          ProjectEntity,
          TaskEntity,
        ];

        return {
          type: 'mariadb',
          database: configService.get('DB_NAME'),
          host: configService.get('DB_HOST', 'localhost'),
          port: configService.get('DB_PORT', 3306),
          username: configService.get('DB_USER'),
          password: configService.get('DB_PASSWORD'),
          entities,
          synchronize: configService.get('SCHEMA_SYNC', false),
        };
      },
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([CustomerEntity, TrackRecordEntity]),
  ],
  controllers: [
    AppController,
    RecordController,
    CustomerController,
    ReportController,
  ],
  providers: [RecordService, CustomerService, ReportService],
})
export class AppModule {}
