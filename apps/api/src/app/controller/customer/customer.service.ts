import { Injectable } from '@nestjs/common';
import { CustomerEntity } from '../../../entities/customer.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../../../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  async getAllByUser(userId: string): Promise<Array<CustomerEntity>> {
    return this.customerRepository
      .createQueryBuilder()
      .relation(UserEntity, 'customers')
      .of(userId)
      .loadMany<CustomerEntity>();
  }

  async getOne(
    // userId: string,
    id: string
  ): Promise<CustomerEntity | undefined> {
    return await this.customerRepository.findOne(id);
  }

  async update(
    // userId: string,
    customerId: string,
    input: Partial<CustomerEntity>
  ): Promise<CustomerEntity> {
    return this.customerRepository.save({ ...input, id: customerId });
  }

  async create(userId: string, input: CustomerEntity) {
    const user = await this.userRepository.findOne(userId);
    if (user) {
      let newCustomer = this.customerRepository.create();
      newCustomer = { ...input, ...newCustomer, user };
      return this.customerRepository.save(newCustomer);
    } else {
      return undefined;
    }
  }

  async remove(userId: string, id: string): Promise<void> {
    await this.customerRepository.delete(id);
  }
}
