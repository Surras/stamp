import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../auth/jwt-auth.guard';
import { CustomerService } from './customer.service';
import { CustomerEntity } from '../../../entities/customer.entity';

@UseGuards(JwtAuthGuard)
@Controller('customers')
export class CustomerController {
  constructor(private customerService: CustomerService) {}

  @Get()
  async findAll(@Request() req): Promise<Array<CustomerEntity>> {
    return this.customerService.getAllByUser(req.user.userId);
  }

  @Post()
  async create(
    @Request() req,
    @Body() input: CustomerEntity
  ): Promise<CustomerEntity> {
    return this.customerService.create(req.user.userId, input);
  }

  @Get(':id')
  async findOne(
    @Request() req,
    @Param('id') id: string
  ): Promise<CustomerEntity | undefined> {
    return this.customerService.getOne(id);
  }

  @Put(':id')
  async update(
    @Request() req,
    @Param('id') id: string,
    @Body() input: CustomerEntity
  ): Promise<CustomerEntity> {
    return this.customerService.update(id, input);
  }

  @Delete(':id')
  async remove(@Request() req, @Param('id') id: string) {
    await this.customerService.remove(req.user.userId, id);
  }
}
