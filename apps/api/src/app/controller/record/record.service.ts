import { Injectable } from '@nestjs/common';
import { TrackRecordEntity } from '../../../entities/track-record.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../../../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomerEntity } from '../../../entities/customer.entity';
import { TrackRecordDto } from '../../../dto/track-record.dto';

@Injectable()
export class RecordService {
  constructor(
    @InjectRepository(TrackRecordEntity)
    private recordRepository: Repository<TrackRecordEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>
  ) {}

  async getAll(userId: string): Promise<Array<TrackRecordEntity>> {
    return this.recordRepository.find({
      where: { user: userId },
      order: { startedAt: 'DESC' },
    });
  }

  async getOne(userId: string, recordId: string): Promise<TrackRecordEntity> {
    return this.recordRepository.findOne(recordId);
  }

  async create(
    userId: string,
    input: TrackRecordDto // TODO besseres Model Handling finden
  ): Promise<TrackRecordEntity> {
    let newRecord = this.recordRepository.create();
    const user = await this.userRepository.findOne(userId);
    const customer =
      input.customer && (await this.customerRepository.findOne(input.customer));

    newRecord = {
      ...newRecord,
      ...input,
      user,
      customer,
      startedAt: input.startedAt || new Date(),
      project: undefined,
      task: undefined,
    };

    return this.recordRepository.save(newRecord);
  }

  async update(
    userId: string,
    id: string,
    input: Partial<TrackRecordDto>
  ): Promise<TrackRecordEntity> {
    const record = await this.recordRepository.findOne(id);
    if (input.startedAt) record.startedAt = input.startedAt;
    if (input.finishedAt) record.finishedAt = input.finishedAt;
    if (input.ticket) record.ticket = input.ticket;
    if (input.workNotes) record.workNotes = input.workNotes;
    if (input.project) record.project = undefined;
    if (record.task) record.task = undefined;

    if (record.customer) {
      record.customer = await this.customerRepository.findOne(input.customer);
    }

    return this.recordRepository.save(record);
  }

  async remove(userId: string, id: string): Promise<void> {
    await this.recordRepository.delete(id);
  }

  async getLatest(userId: string): Promise<TrackRecordEntity | undefined> {
    return await this.recordRepository.findOne({
      where: { user: userId },
      order: { startedAt: 'DESC' },
    });
  }
}
