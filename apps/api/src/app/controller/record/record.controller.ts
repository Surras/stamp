import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';

import { RecordService } from './record.service';
import { JwtAuthGuard } from '../../../auth/jwt-auth.guard';
import { TrackRecordDto } from '../../../dto/track-record.dto';
import { TrackRecordEntity } from '../../../entities/track-record.entity';

@UseGuards(JwtAuthGuard)
@Controller('records')
export class RecordController {
  constructor(private recordService: RecordService) {}

  @Get()
  async findAll(@Request() req): Promise<Array<TrackRecordEntity>> {
    return this.recordService.getAll(req.user.userId);
  }

  @Get('latest')
  async getLatest(@Request() req) {
    const record = await this.recordService.getLatest(req.user.userId);
    return mapEntityToDto(record);
  }

  @Get(':id')
  async findOne(
    @Request() req,
    @Param('id') recordId: string
  ): Promise<TrackRecordEntity> {
    return this.recordService.getOne(req.user.userId, recordId);
  }

  @Post()
  async create(
    @Request() req,
    @Body() input: TrackRecordDto
  ): Promise<TrackRecordDto> {
    const created = await this.recordService.create(req.user.userId, input);
    return mapEntityToDto(created);
  }

  @Put(':id')
  async update(
    @Request() req,
    @Param('id') recordId: string,
    @Body() input: TrackRecordDto
  ): Promise<TrackRecordDto> {
    const updated = await this.recordService.update(
      req.user.userId,
      recordId,
      input
    );
    return mapEntityToDto(updated);
  }

  @Delete(':id')
  async remove(@Request() req, @Param('id') recordId: string) {
    await this.recordService.remove(req.user.userId, recordId);
  }
}

function mapEntityToDto(input: TrackRecordEntity): TrackRecordDto {
  return {
    id: input.id,
    customer: input.customer?.id,
    project: input.project?.id,
    workNotes: input.workNotes,
    ticket: input.ticket,
    startedAt: input.startedAt,
    finishedAt: input.finishedAt,
  };
}
