export interface ReportEntry {
  startedAt?: string;
  finishedAt?: string;
  duration?: number;
  ticket?: string;
  workNotes?: string;
  project?: string;
  customer?: string;
  task?: string;
}
