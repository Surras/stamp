import {
  Controller,
  Get,
  Query,
  Request,
  Response,
  StreamableFile,
  UseGuards,
} from '@nestjs/common';

import { JwtAuthGuard } from '../../../auth/jwt-auth.guard';
import { ReportService } from './report.service';
import { unlink, writeFile } from 'fs/promises';
import { join } from 'path';
import { createReadStream } from 'fs';
import { uuid } from 'uuidv4';

@UseGuards(JwtAuthGuard)
@Controller('report')
export class ReportController {
  constructor(private reportService: ReportService) {}

  @Get('download')
  async downloadReport(
    @Query() query,
    @Request() req,
    @Response({ passthrough: true }) res
  ) {
    const tempFileName = uuid();

    // create report content
    const content = await this.reportService.generateReport(
      req.user.userId,
      query.month,
      query.props && query.props.split(',')
    );

    // write temporary file
    await writeFile(
      join(process.cwd(), `${tempFileName}.json`),
      JSON.stringify(content)
    );
    const streamableFile = new StreamableFile(
      createReadStream(join(process.cwd(), `${tempFileName}.json`))
    );

    res.set({
      'Content-Type': 'application/json',
      'Content-Disposition': 'attachment; filename="report.json"',
    });
    await unlink(join(process.cwd(), `${tempFileName}.json`));
    return streamableFile;
  }

  @Get('/delta')
  async getDelta(@Query() query, @Request() req) {
    return 0;
  }
}
