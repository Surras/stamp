import { Injectable } from '@nestjs/common';
import { TrackRecordEntity } from '../../../entities/track-record.entity';
import { Between, Repository } from 'typeorm';
import { UserEntity } from '../../../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';
import { ReportEntry } from './types';

@Injectable()
export class ReportService {
  constructor(
    @InjectRepository(TrackRecordEntity)
    private recordRepository: Repository<TrackRecordEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  async generateReport(
    userId: string,
    month: number,
    props?: Array<string>
  ): Promise<Array<ReportEntry>> {
    const startTime = DateTime.fromObject({ month: month }).startOf('month');
    const endTime = DateTime.fromObject({ month: month }).endOf('month');
    const dbRes = await this.recordRepository.find({
      where: {
        user: userId,
        startedAt: Between(startTime.toISO(), endTime.toISO()),
      },
      order: { startedAt: 'ASC' },
    });

    return dbRes.map((record) => {
      if (props) {
        return {};
      } else {
        return {
          startedAt: record.startedAt.toISOString(),
          finishedAt: record.finishedAt.toISOString(),
          duration: Math.floor(
            DateTime.fromJSDate(record.finishedAt || new Date())
              .diff(DateTime.fromJSDate(record.startedAt))
              .as('minutes')
          ),
          ticket: record.ticket,
          workNotes: record.workNotes,
        };
      }
    });
  }
}
