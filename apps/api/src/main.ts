import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import * as session from 'express-session';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  const configService = app.get(ConfigService);
  app.setGlobalPrefix(globalPrefix);
  app.enableCors();
  app.use(
    session({
      secret: configService.get('SESSION_SECRET'),
      resave: false,
      saveUninitialized: false,
    })
  );
  const port = process.env.PORT || 80;
  const rootUrl = process.env.ROOT_URL || 'http://localhost';
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: ${rootUrl}:${port}/${globalPrefix}`
  );
}

bootstrap();
