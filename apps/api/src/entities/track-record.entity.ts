import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CustomerEntity } from './customer.entity';
import { ProjectEntity } from './project.entity';
import { TaskEntity } from './task.entity';
import { UserEntity } from './user.entity';

@Entity('TrackRecord')
export class TrackRecordEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  startedAt: Date;

  @Column({ nullable: true })
  finishedAt?: Date;

  @OneToOne(() => CustomerEntity, { nullable: true })
  @JoinColumn()
  customer?: CustomerEntity;

  @OneToOne(() => ProjectEntity, { nullable: true })
  @JoinColumn()
  project?: ProjectEntity;

  @OneToOne(() => TaskEntity, { nullable: true })
  @JoinColumn()
  task?: TaskEntity;

  @Column({ nullable: true })
  ticket?: string;

  @Column({ nullable: true })
  workNotes?: string;

  @ManyToOne(() => UserEntity, (user) => user.trackRecords)
  user: UserEntity;
}
