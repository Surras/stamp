import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { Customer } from '@stamp/api-interfaces';

@Entity('Customer')
export class CustomerEntity implements Customer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @ManyToOne(() => UserEntity, (user) => user.customers)
  user: UserEntity;
}
