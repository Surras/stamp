import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TrackRecordEntity } from './track-record.entity';
import { CustomerEntity } from './customer.entity';
import { ProjectEntity } from './project.entity';
import { TaskEntity } from './task.entity';

@Entity('User')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  password: string;

  @OneToMany(() => TrackRecordEntity, (record) => record.user)
  trackRecords: Array<TrackRecordEntity>;

  @OneToMany(() => CustomerEntity, (customer) => customer.user)
  customers: Array<CustomerEntity>;

  @OneToMany(() => ProjectEntity, (project) => project.user)
  projects: Array<ProjectEntity>;

  @OneToMany(() => TaskEntity, (task) => task.user)
  tasks: Array<TaskEntity>;
}
